//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_IOSLOGGER_H
#define RCCLANIMATION_IOSLOGGER_H

#include "../com/utils/Logger.h"
#include <iostream>

class IOSLogger : public Logger {
public:
    virtual void log(LogLevel level, const char *message) override {
        std::cout << message << std::endl; //TODO Replace for real iOS implementation
    }
};

#endif //RCCLANIMATION_IOSLOGGER_H
