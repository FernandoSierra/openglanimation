//
// Created by Fernando on 19/02/2017.
//

#ifndef RCCLANIMATION_ANIMABLE_H
#define RCCLANIMATION_ANIMABLE_H

#include "Animation.h"

class Animable {
protected:
    Animation *animation = nullptr;
public:
    virtual void setAnimation(Animation *anim) {
        animation = anim;
    }

    void startAnimation(Animation *anim) {
        animation = anim;
        animation->start();
    }

    void startAnimation() {
        if (animation != nullptr) {
            animation->start();
        }
    }
};

#endif //RCCLANIMATION_ANIMABLE_H
