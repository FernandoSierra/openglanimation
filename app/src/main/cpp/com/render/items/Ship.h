//
// Created by Fernando on 11/02/2017.
//

#ifndef RCCLANIMATION_SHIP_H
#define RCCLANIMATION_SHIP_H


#include "../Renderable.h"
#include "../../../libs/glm/gtc/matrix_transform.hpp"

class Ship : public Renderable {
public:
    Ship(){
        modelMatrix = scale(modelMatrix, vec3(0.3f, 0.3f, 0.3f));
        modelMatrix = translate(modelMatrix, vec3(0.0f, 0.0f, 0.1f));
    }
};


#endif //RCCLANIMATION_SHIP_H
