//
// Created by jesus.sierra on 2/03/17.
//

#ifndef RCCLANIMATION_SUN_H
#define RCCLANIMATION_SUN_H

#include "Light.h"

class Sun : public Light {
private:
    typedef enum {
        DAY,
        AFTERNOON,
        NIGHT
    } SunType;
    SunType state;
public:
    Sun() {
        position = vec4(0.0f, 0.0f, 5.0f, 1.0f);
        day();
    }

    void day() {
        ambient = vec4(0.2f, 0.2f, 0.2f, 1.0f);
        diffuse = vec4(0.5f, 0.5f, 0.5f, 1.0f);
        specular = vec4(1.0f, 1.0f, 1.0f, 1.0f);
        shininess = 32.0f;
        state = DAY;
    }

    void afternoon() {
        ambient = vec4(0.095f, 0.095f, 0.095, 1.0f);
        diffuse = vec4(0.6f, 0.6f, 0.6f, 1.0f);
        specular = vec4(1.0f, 0.196078431f, 0.0f, 1.0f);
        shininess = 32.0f;
        state = AFTERNOON;
    }

    void night() {
        ambient = vec4(0.1f, 0.1f, 0.1f, 1.0f);
        diffuse = vec4(0.5f, 0.5f, 0.5f, 1.0f);
        specular = vec4(0.0f, 0.215686275f, 0.564705882f, 1.0f);
        shininess = 20.0f;
        state = NIGHT;
    }

    void next() {
        switch (state) {
            case DAY:
                afternoon();
                break;
            case AFTERNOON:
                night();
                break;
            case NIGHT:
                day();
                break;
        }
    }
};

#endif //RCCLANIMATION_SUN_H
