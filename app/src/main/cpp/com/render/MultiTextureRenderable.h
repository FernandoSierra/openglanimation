//
// Created by Jesús Fernando Sierra Pastrana on 15/02/17.
//

#ifndef RCCLANIMATION_MULTITEXTURERENDERABLE_H
#define RCCLANIMATION_MULTITEXTURERENDERABLE_H


#include "Renderable.h"
#include <dirent.h>
#include <string.h>
#include <vector>
#include "../animation/TextureAnimation.h"

class MultiTextureRenderable : public Renderable {

protected:
public:
    virtual void setAnimation(TextureAnimation *anim) {
        Animable::setAnimation(anim);
        anim->setTexturesTotal(texturesVector.size());
    }

protected:

    virtual void loadTexture(const char *texturePath) {
        vector<const char *> textures;
        struct dirent *entry;
        DIR *dir = opendir(texturePath);
        if (dir == NULL) {
            return;
        }
        while ((entry = readdir(dir)) != NULL) {
            if (strcmp(entry->d_name, ".") != 0 &&
                strcmp(entry->d_name, "..") != 0) {
                char *str = new char[512];
                strcpy(str, texturePath);
                strcat(str, "/");
                strcat(str, entry->d_name);
                textures.push_back(str);
            }
        }
        closedir(dir);
        TextureManager::loadTextures(id, textures);
        texturesVector = TextureManager::getTexturesList(id);
    }

    virtual GLuint getTextureToDisplay() {
        TextureAnimation *textureAnimation = (TextureAnimation *) animation;
        return texturesVector.empty() ?
               0 :
               texturesVector[textureAnimation == nullptr || !textureAnimation->isRunning() ?
                              0 :
                              textureAnimation->getCurrentTexturePosition()];
    }
};


#endif //RCCLANIMATION_MULTITEXTURERENDERABLE_H
