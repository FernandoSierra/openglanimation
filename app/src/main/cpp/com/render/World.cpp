//
// Created by Fernando on 11/02/2017.
//

#include "World.h"

World *World::instance = nullptr;

World::World() {
    camera = Camera::getInstance();
    projectionMatrix = mat4(1.0f);
    sun = new Sun();
}

World *World::getInstance() {
    if (instance == nullptr) {
        instance = new World();
    }
    return instance;
}

void World::draw(Renderable *renderable) {
    if (renderable != nullptr) {
        renderable->draw(projectionMatrix);
    }
}

void World::draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.1f, 0.2f, 0.4f, 1.0f);
    if (water != nullptr) {
        water->startAnimation();
    }
    draw(water);
    draw(land);
    draw(ship);
}

void World::setWater(Water *water) {
    World::water = water;

    TextureAnimation *animation = new TextureAnimation();
    animation->setRepetitionMode(Animation::INFINITE_REVERSE);
    animation->setDuration(4000);
    World::water->setAnimation(animation);
    World::water->setWorldLight(sun);
}

void World::setLand(Land *land) {
    World::land = land;
    World::land->setWorldLight(sun);
}

void World::setShip(Ship *ship) {
    World::ship = ship;
    World::ship->setWorldLight(sun);
}

Ship *World::getShip() const {
    return ship;
}

Water *World::getWater() const {
    return water;
}

void World::setProjectionMatrix(int width, int height) {
    screenWidth = width;
    screenHeight = height;
    projectionMatrix = perspective(radians(45.0f), (float) width / (float) height, 1.0f, 10.f);
}

World::~World() {
    camera->~Camera();
    if (water != nullptr) {
        water->~Water();
    }
    if (ship != nullptr) {
        ship->~Ship();
    }
    if (land != nullptr) {
        land->~Land();
    }
}

int World::click(float x, float y) {
    sun->next();
    vec3 rayOrigin, rayDirection;
    OBBCollision::screenPositionToWorldRay(
            x,
            y,
            screenWidth,
            screenHeight,
            Camera::getInstance()->getViewMatrix(),
            projectionMatrix,
            rayOrigin,
            rayDirection);
    float intersectionDistance;
    bool result = OBBCollision::testRayObbIntersection(
            rayOrigin,
            rayDirection,
            vec3(-3.0f, -3.0f, 0.0f),
            vec3(3.0f, 3.0f, 0.0f),
            ship->getModelMatrix(),
            intersectionDistance);
    return result ? 1 : 0;
}
