//
// Created by Fernando on 12/02/2017.
//

#ifndef RCCLANIMATION_OPENGLUTILS_H
#define RCCLANIMATION_OPENGLUTILS_H

#include <vector>
#include <stdio.h>
#include <string>
#include "Logger.h"
#include "../../libs/glm/glm.hpp"
#include "../../PlatformProvider.h"

using namespace std;
using namespace glm;

static Logger *logger = PlatformProvider::getLogger();

class OpenGLUtils {
private:
    OpenGLUtils() {}

public:

    static bool loadObj(const char *objPath,
                        vector<vec3> &outVertexes,
                        vector<vec3> &outNormals,
                        vector<vec2> &outUvs) {
        vector<unsigned int> vertexIndexes, uvIndexes, normalIndexes;
        vector<vec3> tempVertexes;
        vector<vec2> tempUvs;
        vector<vec3> tempNormals;

        FILE *file = fopen(objPath, "r");
        if (file == NULL) {
            logger->log(Logger::ERROR, "Invalid OBJ file");
            return false;
        }

        char lineHeader[1024];
        int res = fscanf(file, "%s", lineHeader);;
        while (res != EOF) {
            if (strcmp(lineHeader, "v") == 0) { // Vertex line
                vec3 vertex;
                fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
                tempVertexes.push_back(vertex);
            } else if (strcmp(lineHeader, "vt") == 0) { //Texel line
                vec2 uv;
                fscanf(file, "%f %f\n", &uv.x, &uv.y);
                tempUvs.push_back(uv);
            } else if (strcmp(lineHeader, "vn") == 0) { //Normal line
                vec3 normal;
                fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
                tempNormals.push_back(normal);
            } else if (strcmp(lineHeader, "f") == 0) { //Face line
                string vertex1, vertex2, vertex3;
                unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
                int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0],
                                     &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1],
                                     &normalIndex[1], &vertexIndex[2], &uvIndex[2],
                                     &normalIndex[2]);
                if (matches != 9) {
                    logger->log(Logger::ERROR, "Error while parsing faces");
                    return false;
                }
                vertexIndexes.push_back(vertexIndex[0]);
                vertexIndexes.push_back(vertexIndex[1]);
                vertexIndexes.push_back(vertexIndex[2]);
                uvIndexes.push_back(uvIndex[0]);
                uvIndexes.push_back(uvIndex[1]);
                uvIndexes.push_back(uvIndex[2]);
                normalIndexes.push_back(normalIndex[0]);
                normalIndexes.push_back(normalIndex[1]);
                normalIndexes.push_back(normalIndex[2]);
            }
            res = fscanf(file, "%s", lineHeader);
        }

        for (unsigned int i = 0; i < vertexIndexes.size(); i++) {
            unsigned int vertexIndex = vertexIndexes[i];
            vec3 vertex = tempVertexes[vertexIndex - 1];
            outVertexes.push_back(vertex);
        }

        for (unsigned int i = 0; i < normalIndexes.size(); i++) {
            unsigned int normalIndex = normalIndexes[i];
            vec3 normal = tempNormals[normalIndex - 1];
            outNormals.push_back(normal);
        }

        for (unsigned int i = 0; i < uvIndexes.size(); i++) {
            unsigned int uvIndex = uvIndexes[i];
            vec2 uv = tempUvs[uvIndex - 1];
            outUvs.push_back(uv);
        }
        return true;
    }

    template<typename T>
    static GLuint createBuffer(const vector<T> &input) {
        GLuint bufferId;
        glGenBuffers(1, &bufferId);
        glBindBuffer(GL_ARRAY_BUFFER, bufferId);
        glBufferData(GL_ARRAY_BUFFER, input.size() * sizeof(T), &input[0], GL_STATIC_DRAW);
        return bufferId;
    }
};

#endif //RCCLANIMATION_OPENGLUTILS_H
