//
// Created by Fernando on 11/02/2017.
//

#include "AnimationApi.h"

void AnimationApi::initOpenGL() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_ALPHA);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);
}

void AnimationApi::setViewPort(int width, int height) {
    glViewport(0, 0, width, height);
    world->setProjectionMatrix(width, height);
}

bool AnimationApi::loadWater(const char *objPath,
                             const char *texturePath) {
    Water *water = new Water();
    bool result = water->load(objPath, texturePath);
    world->setWater(water);
    return result;
}

bool AnimationApi::loadShip(const char *objPath,
                            const char *texturePath) {
    Ship *ship = new Ship();
    bool result = ship->load(objPath, texturePath);
    world->setShip(ship);
    return result;
}

void AnimationApi::draw() {
    world->draw();
}

AnimationApi::~AnimationApi() {
    glFlush();
    glFinish();
    world->~World();
}

int AnimationApi::click(float x, float y) {
    return world->click(x, y);
}
