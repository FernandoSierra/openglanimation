package com.rccl.openglanimation;

import android.opengl.GLSurfaceView;
import android.os.Handler;

/**
 * @author Fernando Sierra Pastrana
 * @version 1.0
 * @since 27/02/17.
 */
public class GLSurfaceViewLifecycleHelper implements Runnable {
    private static final int DELAY_MILLIS = 120000; // 2 Minutes
    private GLSurfaceView surfaceView;
    private Handler handler;
    private boolean wasPaused;

    public GLSurfaceViewLifecycleHelper(GLSurfaceView surfaceView) {
        this.surfaceView = surfaceView;
        handler = new Handler();
    }

    public void onPause() {
        handler.postDelayed(this, DELAY_MILLIS);
        wasPaused = false;
    }

    public void onResume() {
        if (wasPaused) {
            surfaceView.onResume();
        } else {
            handler.removeCallbacks(this);
        }
    }

    public void onDestroy() {
        if (!wasPaused) {
            handler.removeCallbacks(this);
        }
    }

    @Override
    public void run() {
        if (surfaceView != null) {
            surfaceView.onPause();
        }
        wasPaused = true;
    }
}
